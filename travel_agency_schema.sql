-- DROP TABLE country;

CREATE TABLE country (
	id int8 NOT NULL,
	country_code varchar(2) NOT NULL,
	country_name varchar NULL,
	"version" int4 NULL,
	active bool NOT NULL,
	CONSTRAINT country_pk PRIMARY KEY (id)
);

-- DROP TABLE hotel;

CREATE TABLE hotel (
	id int8 NOT NULL,
	hotel_code varchar NOT NULL,
	hotel_name varchar(255) NULL,
	country_id int8 NULL,
	"version" int4 NULL,
	active bool NOT NULL,
	CONSTRAINT hotel_pk PRIMARY KEY (id)
);

-- DROP TABLE "user";

CREATE TABLE "user" (
	id int8 NOT NULL,
	firstname varchar(255) NOT NULL,
	lastname varchar(255) NOT NULL,
	"version" int4 NULL,
	active bool NOT NULL,
	CONSTRAINT user_pk PRIMARY KEY (id)
);

-- DROP TABLE tour;

CREATE TABLE tour (
	id int8 NOT NULL,
	tour_code varchar(20) NOT NULL,
	tour_name varchar(255) NULL,
	hotel_id varchar NULL,
	start_date date NOT NULL,
	end_date date NOT NULL,
	"version" int4 NULL,
	active bool NOT NULL,
	CONSTRAINT tour_pk PRIMARY KEY (id)
);

-- DROP TABLE "order";

CREATE TABLE "order" (
	id int8 NOT NULL,
	order_numder int8 NOT NULL,
	start_date date NOT NULL,
	end_date date NOT NULL,
	user_id int8 NOT NULL,
	hotel_id int8 NOT NULL,
	"version" int4 NULL,
	active bool NOT NULL,
	CONSTRAINT order_pk PRIMARY KEY (id)
);

-- DROP TABLE review;

CREATE TABLE review (
	id int8 NOT NULL,
	order_number int8 NULL,
	message text NOT NULL,
	active bool NOT NULL,
	CONSTRAINT review_pk PRIMARY KEY (id)
);



ALTER TABLE public.hotel ADD CONSTRAINT hotel_fk FOREIGN KEY (id) REFERENCES country(id);
ALTER TABLE public.tour ADD CONSTRAINT tour_fk FOREIGN KEY (id) REFERENCES hotel(id);
ALTER TABLE public."order" ADD CONSTRAINT order_fk FOREIGN KEY (id) REFERENCES hotel(id);
ALTER TABLE public."order" ADD CONSTRAINT order_fk_1 FOREIGN KEY (id) REFERENCES "user"(id);
ALTER TABLE public.review ADD CONSTRAINT review_fk FOREIGN KEY (id) REFERENCES "order"(id);

CREATE SEQUENCE country_id_seq;
ALTER TABLE country ALTER COLUMN id SET DEFAULT NEXTVAL('country_id_seq'::regclass);
CREATE SEQUENCE hotel_id_seq;
ALTER TABLE hotel ALTER COLUMN id SET DEFAULT NEXTVAL('hotel_id_seq'::regclass);
CREATE SEQUENCE user_id_seq;
ALTER TABLE usr ALTER COLUMN id SET DEFAULT NEXTVAL('user_id_seq'::regclass);
CREATE SEQUENCE tour_id_seq;
ALTER TABLE tour ALTER COLUMN id SET DEFAULT NEXTVAL('tour_id_seq'::regclass);
CREATE SEQUENCE order_id_seq;
ALTER TABLE ordr ALTER COLUMN id SET DEFAULT NEXTVAL('order_id_seq'::regclass);
CREATE SEQUENCE review_id_seq;
ALTER TABLE review ALTER COLUMN id SET DEFAULT NEXTVAL('review_id_seq'::regclass);