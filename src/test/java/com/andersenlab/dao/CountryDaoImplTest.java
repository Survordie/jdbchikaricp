package com.andersenlab.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import com.andersenlab.dao.CountryDaoImpl;
import com.andersenlab.model.Country;

public class CountryDaoImplTest {

  private Optional<Country> country = null;
  private Optional<Country> countryForUpdate = null;
  private CountryDaoImpl daoCountry = null;

  @Before
  public void setUp() throws Exception {
    country = Optional.of(new Country(0L, 0, "ru", "Russia", true));
    countryForUpdate = Optional.of(new Country(0L, 0, "ru", "RussianFederation", true));
    daoCountry = new CountryDaoImpl();
  }

  @Test
  public void testCreateCountry() {
    daoCountry.createEntity(country);
    List<Optional<?>> allCountry = daoCountry.getAllEntity();
    assertFalse(allCountry.isEmpty());
  }

  @Test
  public void testGetAllCountry() {
    daoCountry.createEntity(country);
    List<Optional<?>> allCountry = daoCountry.getAllEntity();
    assertFalse(allCountry.isEmpty());
  }

  @Test
  public void testGetCountry() {
    Long index = daoCountry.createEntity(country);
    Optional<?> country2 = daoCountry.getEntityById(index);
    assertEquals(index, ((Country) country2.get()).getId());
  }

  @Test
  public void testUpdateCountry() {
    Long index = daoCountry.createEntity(country);
    countryForUpdate.get().setId(index);
    daoCountry.updateEntity(countryForUpdate);
  }

  @Test
  public void testDeleteCountry() {
    daoCountry.clearEntityList();
    List<Optional<?>> allCountry = daoCountry.getAllEntity();
    assertTrue(allCountry.isEmpty());
  }

}
