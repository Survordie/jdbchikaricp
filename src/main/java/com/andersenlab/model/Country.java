package com.andersenlab.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

/**
 * Country
 *
 */
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Country {

  private Long id;
  private Integer version;
  private String country_code;
  private String country_name;
  private Boolean active;

}
