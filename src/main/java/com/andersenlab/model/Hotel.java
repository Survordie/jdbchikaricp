package com.andersenlab.model;

import lombok.Data;

@Data
public class Hotel {

  private Long id;
  private Integer version;
  private String hotel_code;
  private String hotel_name;
  private Long country_id;
  private Boolean active;
}
