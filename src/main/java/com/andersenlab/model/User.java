package com.andersenlab.model;

import lombok.Data;

@Data
public class User {

  private Long id;
  private Integer version;
  private String firstname;
  private String lastname;
  private Boolean active;
 
}
