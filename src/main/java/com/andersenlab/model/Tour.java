package com.andersenlab.model;

import java.time.LocalDate;
import lombok.Data;

@Data
public class Tour {

  private Long id;
  private Integer version;
  private String tour_code;
  private String tour_name;
  private Long hotel_id;
  private LocalDate start_date;
  private LocalDate end_date;
  private Boolean active;
}
