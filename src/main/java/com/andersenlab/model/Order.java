package com.andersenlab.model;

import java.time.LocalDate;
import lombok.Data;

@Data
public class Order {
  private Long id;
  private Integer version;
  private String order_numder;
  private LocalDate start_date;
  private LocalDate end_date;
  private Long user_id;
  private Long hotel_id;
  private Boolean active;

}
