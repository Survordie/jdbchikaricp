package com.andersenlab.model;

import lombok.Data;

@Data
public class Review {

  private Long id;
  private Integer version;
  private String order_numder;
  private String message;
  private Boolean active;
}
