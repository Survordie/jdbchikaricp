package com.andersenlab.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.andersenlab.DBUtil;
import com.andersenlab.model.Order;

public class OrderDaoImpl implements OrderDao {

	private DataSource dataSource = null;
	private Connection connection = null;
	private PreparedStatement pstmt = null;
	private ResultSet resultSet = null;
	  
	  
	public OrderDaoImpl() {
		this.dataSource = DBUtil.getConnection();
	}

	@Override
	public Long createOrder(Order order) {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement(
		          "INSERT INTO order(order_numder,start_date, end_date, user_id, hotel_id, active) VALUES(?,?,?,?,?,?)",
		          Statement.RETURN_GENERATED_KEYS);

		      pstmt.setString(1, order.getOrder_numder());
		      pstmt.setDate(2, (Date) Date.from(order.getStart_date().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		      pstmt.setDate(3, (Date) Date.from(order.getEnd_date().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		      pstmt.setLong(4, order.getUser_id());
		      pstmt.setLong(5, order.getUser_id());
		      pstmt.setBoolean(6, true);

		      boolean execute = pstmt.execute();

		      ResultSet generatedKeys = pstmt.getGeneratedKeys();
		      
		      while (generatedKeys.next()) {
		        return generatedKeys.getLong(1);

		      }      
		    } catch (SQLException e) {
		      e.printStackTrace();
		    }
		    return null;
	}

	@Override
	public List<Order> getAllOrder() {
		List<Order> listOrder = new ArrayList<>();
	    try {
	      connection = dataSource.getConnection();
	      pstmt = connection.prepareStatement("SELECT * FROM hotel");
	      resultSet = pstmt.executeQuery();

	      while (resultSet.next()) {
	    	  Order order = new Order();
	    	  order.setId(resultSet.getLong(1));
	    	  order.setOrder_numder(resultSet.getString(2));
	    	  order.setStart_date(Instant.ofEpochMilli(resultSet.getDate(3).getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
	    	  order.setEnd_date(Instant.ofEpochMilli(resultSet.getDate(4).getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
	    	  order.setUser_id(resultSet.getLong(5));
	    	  order.setHotel_id(resultSet.getLong(6));
	    	  order.setActive(resultSet.getBoolean(8));
	        listOrder.add(order);

	      }
	    } catch (SQLException e) {

	      e.printStackTrace();
	    }
	    return listOrder;
	}

	@Override
	public Order getOrderById(Long id) {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement("SELECT * FROM order WHERE id = ?");
		      pstmt.setLong(1, id);
		      resultSet = pstmt.executeQuery();
		      while (resultSet.next()) {
		    	  Order order = new Order();
		    	  order.setId(resultSet.getLong(1));
		    	  order.setOrder_numder(resultSet.getString(2));
		    	  order.setStart_date(Instant.ofEpochMilli(resultSet.getDate(3).getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
		    	  order.setEnd_date(Instant.ofEpochMilli(resultSet.getDate(4).getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
		    	  order.setUser_id(resultSet.getLong(5));
		    	  order.setHotel_id(resultSet.getLong(6));
		    	  order.setActive(resultSet.getBoolean(8));
		        
		        return order;

		      }

		    } catch (SQLException e) {
		      e.printStackTrace();
		    }

		    return null;
	}

	@Override
	public void updateOrder(Order order) {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement(
		          "UPDATE order SET order_numder = ?, start_date = ?, end_date, user_id = ?, hotel_id, active = ?  WHERE id = ?");
		      pstmt.setString(1, order.getOrder_numder());
		      pstmt.setDate(2, (Date) Date.from(order.getStart_date().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		      pstmt.setDate(3, (Date) Date.from(order.getEnd_date().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		      pstmt.setLong(4, order.getUser_id());
		      pstmt.setLong(6, order.getHotel_id());
		      pstmt.setBoolean(7, order.getActive() == null ? false : order.getActive());
		      pstmt.setLong(8, order.getId());

		      pstmt.executeUpdate();
		    } catch (SQLException e) {

		      e.printStackTrace();
		    }
		
	}

	@Override
	public void deleteOrderById(Long id) {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement("DELETE FROM order WHERE id = ?");
		      pstmt.setLong(1, id);

		      pstmt.execute();
		    } catch (SQLException e) {

		      e.printStackTrace();
		    }
		
	}

	@Override
	public void clearAllOrder() {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement("DELETE FROM order");

		      pstmt.execute();
		    } catch (SQLException e) {

		      e.printStackTrace();
		    }
		
	}

}
