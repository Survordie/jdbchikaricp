package com.andersenlab.dao;

import java.util.List;
import com.andersenlab.model.Tour;

public interface TourDao {

	public Long createTour(Tour tour);
	public List<Tour> getAllTour();
	public Tour getTourById(Long id);
	public void updateTour(Tour tour);
	public void deleteTourById(Long id);
	public void clearAllTour();
}
