package com.andersenlab.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.andersenlab.DBUtil;
import com.andersenlab.model.Order;
import com.andersenlab.model.Tour;

public class TourDaoImpl implements TourDao {

	private DataSource dataSource = null;
	private Connection connection = null;
	private PreparedStatement pstmt = null;
	private ResultSet resultSet = null;

	public TourDaoImpl() {
		this.dataSource = DBUtil.getConnection();
	}

	@Override
	public Long createTour(Tour tour) {

		try {
			connection = dataSource.getConnection();
			pstmt = connection.prepareStatement(
					"INSERT INTO tour(tour_code,tour_name, hotel_id, start_date, end_date, active) VALUES(?,?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);

			pstmt.setString(1, tour.getTour_code());
			pstmt.setDate(4,
					(Date) Date.from(tour.getStart_date().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
			pstmt.setDate(5,
					(Date) Date.from(tour.getEnd_date().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
			pstmt.setString(2, tour.getTour_name());
			pstmt.setLong(3, tour.getId());
			pstmt.setBoolean(6, true);

			boolean execute = pstmt.execute();

			ResultSet generatedKeys = pstmt.getGeneratedKeys();

			while (generatedKeys.next()) {
				return generatedKeys.getLong(1);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Tour> getAllTour() {
		List<Tour> listTour = new ArrayList<>();
	    try {
	      connection = dataSource.getConnection();
	      pstmt = connection.prepareStatement("SELECT * FROM tour");
	      resultSet = pstmt.executeQuery();

	      while (resultSet.next()) {
	    	  Tour tour = new Tour();
	    	  tour.setId(resultSet.getLong(1));
	    	  tour.setTour_code(resultSet.getString(2));
	    	  tour.setStart_date(Instant.ofEpochMilli(resultSet.getDate(4).getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
	    	  tour.setEnd_date(Instant.ofEpochMilli(resultSet.getDate(5).getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
	    	  tour.setTour_name(resultSet.getString(3));
	    	  tour.setHotel_id(resultSet.getLong(6));
	    	  tour.setActive(resultSet.getBoolean(8));
	    	  listTour.add(tour);

	      }
	    } catch (SQLException e) {

	      e.printStackTrace();
	    }
	    return listTour;
	}

	@Override
	public Tour getTourById(Long id) {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement("SELECT * FROM order WHERE id = ?");
		      pstmt.setLong(1, id);
		      resultSet = pstmt.executeQuery();
		      while (resultSet.next()) {
		    	  Tour tour = new Tour();
		    	  tour.setId(resultSet.getLong(1));
		    	  tour.setTour_code(resultSet.getString(2));
		    	  tour.setStart_date(Instant.ofEpochMilli(resultSet.getDate(4).getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
		    	  tour.setEnd_date(Instant.ofEpochMilli(resultSet.getDate(5).getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
		    	  tour.setTour_name(resultSet.getString(3));
		    	  tour.setHotel_id(resultSet.getLong(6));
		    	  tour.setActive(resultSet.getBoolean(8));
		        
		        return tour;

		      }

		    } catch (SQLException e) {
		      e.printStackTrace();
		    }

		    return null;
	}

	@Override
	public void updateTour(Tour tour) {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement(
		          "UPDATE tour SET tour_code = ?, start_date = ?, end_date, tour_name = ?, hotel_id, active = ?  WHERE id = ?");
		      pstmt.setString(1, tour.getTour_code());
		      pstmt.setDate(2, (Date) Date.from(tour.getStart_date().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		      pstmt.setDate(3, (Date) Date.from(tour.getEnd_date().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		      pstmt.setString(4, tour.getTour_name());
		      pstmt.setLong(6, tour.getHotel_id());
		      pstmt.setBoolean(7, tour.getActive() == null ? false : tour.getActive());
		      pstmt.setLong(8, tour.getId());

		      pstmt.executeUpdate();
		    } catch (SQLException e) {

		      e.printStackTrace();
		    }

	}

	@Override
	public void deleteTourById(Long id) {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement("DELETE FROM tour WHERE id = ?");
		      pstmt.setLong(1, id);

		      pstmt.execute();
		    } catch (SQLException e) {

		      e.printStackTrace();
		    }

	}

	@Override
	public void clearAllTour() {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement("DELETE FROM tour");

		      pstmt.execute();
		    } catch (SQLException e) {

		      e.printStackTrace();
		    }

	}

}
