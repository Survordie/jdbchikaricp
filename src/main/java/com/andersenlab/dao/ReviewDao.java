package com.andersenlab.dao;

import java.util.List;
import com.andersenlab.model.Review;

public interface ReviewDao {

	public Long createReview(Review review);
	public List<Review> getAllReview();
	public Review getReviewById(Long id);
	public void updateReview(Review review);
	public void deleteReviewById(Long id);
	public void clearAllReview();
}
