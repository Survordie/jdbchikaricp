package com.andersenlab.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.andersenlab.DBUtil;
import com.andersenlab.model.Hotel;

public class HotelDaoImpl implements HotelDao {

	private DataSource dataSource = null;
	private Connection connection = null;
	private PreparedStatement pstmt = null;
	private ResultSet resultSet = null;

	public HotelDaoImpl() {
		this.dataSource = DBUtil.getConnection();
	}

	@Override
	public Long createHotel(Hotel hotel) {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement(
		          "INSERT INTO hotel(hotel_code,hotel_name, country_id,active) VALUES(?,?,?,?)",
		          Statement.RETURN_GENERATED_KEYS);

		      pstmt.setString(1, hotel.getHotel_code());
		      pstmt.setString(2, hotel.getHotel_name());
		      pstmt.setLong(3, hotel.getCountry_id());
		      pstmt.setBoolean(4, true);

		      boolean execute = pstmt.execute();

		      ResultSet generatedKeys = pstmt.getGeneratedKeys();
		      
		      while (generatedKeys.next()) {
		        return generatedKeys.getLong(1);

		      }      
		    } catch (SQLException e) {
		      e.printStackTrace();
		    }
		    return null;
	}

	@Override
	public List<Hotel> getAllHotel() {
		List<Hotel> listHotel = new ArrayList<>();
	    try {
	      connection = dataSource.getConnection();
	      pstmt = connection.prepareStatement("SELECT * FROM hotel");
	      resultSet = pstmt.executeQuery();

	      while (resultSet.next()) {
	        Hotel hotel = new Hotel();
	        hotel.setId(resultSet.getLong(1));
	        hotel.setHotel_code(resultSet.getString(2));
	        hotel.setHotel_name(resultSet.getString(3) != null ? resultSet.getString(3) : "");
	        hotel.setCountry_id(resultSet.getLong(5));
	        hotel.setActive(resultSet.getBoolean(6));
	        listHotel.add(hotel);

	      }
	    } catch (SQLException e) {

	      e.printStackTrace();
	    }
	    return listHotel;
	}

	@Override
	public Hotel getHotelById(Long id) {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement("SELECT * FROM hotel WHERE id = ?");
		      pstmt.setLong(1, id);
		      resultSet = pstmt.executeQuery();
		      while (resultSet.next()) {
		        Hotel hotel = new Hotel();
		        hotel.setId(resultSet.getLong(1));
		        hotel.setHotel_code(resultSet.getString(2));
		        hotel.setHotel_name(resultSet.getString(3) != null ? resultSet.getString(3) : "");
		        hotel.setCountry_id(resultSet.getLong(5));
		        hotel.setActive(resultSet.getBoolean(6));

		        return hotel;

		      }

		    } catch (SQLException e) {
		      e.printStackTrace();
		    }

		    return null;
	}

	@Override
	public void updateHotel(Hotel hotel) {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement(
		          "UPDATE hotel SET hotel_code = ?, hotel_name = ?, country_id = ?, active = ?  WHERE id = ?");
		      pstmt.setString(1, hotel.getHotel_code().isEmpty() ? "ru" : hotel.getHotel_code());
		      pstmt.setString(2, hotel.getHotel_name());
		      pstmt.setLong(3, hotel.getCountry_id());
		      pstmt.setBoolean(4, hotel.getActive() == null ? false : hotel.getActive());
		      pstmt.setLong(5, hotel.getId());

		      pstmt.executeUpdate();
		    } catch (SQLException e) {

		      e.printStackTrace();
		    }


	}

	@Override
	public void deleteHotelById(Long id) {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement("DELETE FROM hotel WHERE id = ?");
		      pstmt.setLong(1, id);

		      pstmt.execute();
		    } catch (SQLException e) {

		      e.printStackTrace();
		    }

	}

	@Override
	public void clearAllHotel() {
		try {
		      connection = dataSource.getConnection();
		      pstmt = connection.prepareStatement("DELETE FROM hotel");

		      pstmt.execute();
		    } catch (SQLException e) {

		      e.printStackTrace();
		    }

	}

}
