package com.andersenlab.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.sql.DataSource;
import com.andersenlab.DBUtil;
import com.andersenlab.model.Country;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CountryDaoImpl implements CommonDaoInterface {

  private DataSource dataSource = null;
  private PreparedStatement pstmt = null;
  private ResultSet resultSet = null;

  public CountryDaoImpl() {
    this.dataSource = DBUtil.getConnection();
  }

  @Override
  public Long createEntity(Optional<?> entity) {
    log.debug("Entered to createEntity function");
    try (Connection connection = dataSource.getConnection()) {

      pstmt = connection.prepareStatement(
          "INSERT INTO country(country_code,country_name,active) VALUES(?,?,?)",
          Statement.RETURN_GENERATED_KEYS);

      Country country = (Country) entity.get();
      pstmt.setString(1, country.getCountry_code());
      pstmt.setString(2, country.getCountry_name());
      pstmt.setBoolean(3, true);

      boolean execute = pstmt.execute();

      ResultSet generatedKeys = pstmt.getGeneratedKeys();

      while (generatedKeys.next()) {
        log.debug("Leaving createEntity function");
        return generatedKeys.getLong(1);

      }
    } catch (SQLException e) {
      log.error("Error trying to insert message into database" + e.getMessage());
    }
    log.debug("Leaving createEntity function");
    return null;
  }

  @Override
  public List<Optional<?>> getAllEntity() {
    log.debug("Entered to getAllEntity function");
    List<Country> listCountry = new ArrayList<>();
    List<Optional<?>> resultList = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {

      pstmt = connection.prepareStatement("SELECT * FROM country");
      resultSet = pstmt.executeQuery();

      while (resultSet.next()) {
        Country country = new Country();
        country.setId(resultSet.getLong(1));
        country.setCountry_code(resultSet.getString(2));
        country.setCountry_name(resultSet.getString(3) != null ? resultSet.getString(3) : "");
        country.setActive(resultSet.getBoolean(5));
        listCountry.add(country);

      }
      resultList = listCountry.stream().map(Optional::ofNullable).collect(Collectors.toList());
    } catch (SQLException e) {

      log.error("Error trying to get messages from database" + e.getMessage());
    }
    log.debug("Leaving getAllEntity function");
    return resultList;
  }

  @Override
  public Optional<?> getEntityById(Long id) {
    log.debug("Entered to getEntityById function");
    try (Connection connection = dataSource.getConnection()) {

      pstmt = connection.prepareStatement("SELECT * FROM country WHERE id = ?");
      pstmt.setLong(1, id);
      resultSet = pstmt.executeQuery();
      while (resultSet.next()) {
        Country country = new Country();
        country.setId(resultSet.getLong(1));
        country.setCountry_code(resultSet.getString(2));
        country.setCountry_name(resultSet.getString(3) != null ? resultSet.getString(3) : "");
        country.setActive(resultSet.getBoolean(5));

        log.debug("Leaving getEntityById function");
        return Optional.ofNullable(country);

      }

    } catch (SQLException e) {
      log.error("Error trying to get message by Id from database" + e.getMessage());
    }
    log.debug("Leaving getEntityById function");
    return null;
  }

  @Override
  public void updateEntity(Optional<?> entity) {
    log.debug("Entered to updateEntity function");
    try (Connection connection = dataSource.getConnection()) {

      pstmt = connection.prepareStatement(
          "UPDATE country SET country_code = ?, country_name = ?, active = ?  WHERE id = ?");

      Country country = (Country) entity.get();
      pstmt.setString(1, country.getCountry_code().isEmpty() ? "ru" : country.getCountry_code());
      pstmt.setString(2, country.getCountry_name());
      pstmt.setBoolean(3, country.getActive() == null ? false : country.getActive());
      pstmt.setLong(4, country.getId());

      pstmt.executeUpdate();
    } catch (SQLException e) {

      log.error("Error trying to update message in database" + e.getMessage());
    }
    log.debug("Leaving updateEntity function");
  }

  @Override
  public void deleteEntity(Long id) {
    log.debug("Entered to deleteEntity function");
    try (Connection connection = dataSource.getConnection()) {

      pstmt = connection.prepareStatement("DELETE FROM country WHERE id = ?");
      pstmt.setLong(1, id);

      pstmt.execute();
    } catch (SQLException e) {

      e.printStackTrace();
    }

  }

  @Override
  public void clearEntityList() {
    try (Connection connection = dataSource.getConnection()) {

      pstmt = connection.prepareStatement("DELETE FROM country");

      pstmt.execute();
    } catch (SQLException e) {

      log.error("Error trying to clear all messages in database" + e.getMessage());
    }
    log.debug("Leaving clearEntityList function");
  }

}
