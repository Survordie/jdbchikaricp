package com.andersenlab.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.andersenlab.DBUtil;
import com.andersenlab.model.Order;
import com.andersenlab.model.Review;

public class ReviewDaoImpl implements ReviewDao {

	private DataSource dataSource = null;
	private Connection connection = null;
	private PreparedStatement pstmt = null;
	private ResultSet resultSet = null;

	public ReviewDaoImpl() {
		this.dataSource = DBUtil.getConnection();
	}

	@Override
	public Long createReview(Review review) {
		try {
			connection = dataSource.getConnection();
			pstmt = connection.prepareStatement("INSERT INTO review(order_numder,message, active) VALUES(?,?,?)",
					Statement.RETURN_GENERATED_KEYS);

			pstmt.setString(1, review.getOrder_numder());
			;
			pstmt.setString(2, review.getMessage());
			pstmt.setBoolean(3, true);

			boolean execute = pstmt.execute();

			ResultSet generatedKeys = pstmt.getGeneratedKeys();

			while (generatedKeys.next()) {
				return generatedKeys.getLong(1);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Review> getAllReview() {
		List<Review> listReview = new ArrayList<>();
		try {
			connection = dataSource.getConnection();
			pstmt = connection.prepareStatement("SELECT * FROM hotel");
			resultSet = pstmt.executeQuery();

			while (resultSet.next()) {
				Review review = new Review();
				review.setId(resultSet.getLong(1));
				review.setOrder_numder(resultSet.getString(2));

				review.setMessage(resultSet.getString(3));
				review.setActive(resultSet.getBoolean(5));
				listReview.add(review);

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return listReview;
	}

	@Override
	public Review getReviewById(Long id) {
		try {
			connection = dataSource.getConnection();
			pstmt = connection.prepareStatement("SELECT * FROM order WHERE id = ?");
			pstmt.setLong(1, id);
			resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				Review review = new Review();
				review.setId(resultSet.getLong(1));
				review.setOrder_numder(resultSet.getString(2));

				review.setMessage(resultSet.getString(3));
				review.setActive(resultSet.getBoolean(5));

				return review;

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public void updateReview(Review review) {
		try {
			connection = dataSource.getConnection();
			pstmt = connection
					.prepareStatement("UPDATE review SET order_numder = ?, message = ?, active = ?  WHERE id = ?");
			pstmt.setString(1, review.getOrder_numder());

			pstmt.setString(2, review.getMessage());
			pstmt.setBoolean(3, review.getActive() == null ? false : review.getActive());
			pstmt.setLong(8, review.getId());

			pstmt.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	@Override
	public void deleteReviewById(Long id) {
		try {
			connection = dataSource.getConnection();
			pstmt = connection.prepareStatement("DELETE FROM review WHERE id = ?");
			pstmt.setLong(1, id);

			pstmt.execute();
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void clearAllReview() {
		try {
			connection = dataSource.getConnection();
			pstmt = connection.prepareStatement("DELETE FROM review");

			pstmt.execute();
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

}
