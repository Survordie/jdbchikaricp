package com.andersenlab.dao;

import java.util.List;
import java.util.Optional;

public interface CommonDaoInterface {

  public Long createEntity(Optional<?> entity);
  public List<Optional<?>> getAllEntity();
  public Optional<?> getEntityById(Long id);
  public void updateEntity(Optional<?> entity);
  public void deleteEntity(Long id);
  public void clearEntityList();
}
