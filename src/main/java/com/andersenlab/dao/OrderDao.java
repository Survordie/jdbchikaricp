package com.andersenlab.dao;

import java.util.List;
import com.andersenlab.model.Order;

public interface OrderDao {

	public Long createOrder(Order order);
	public List<Order> getAllOrder();
	public Order getOrderById(Long id);
	public void updateOrder(Order order);
	public void deleteOrderById(Long id);
	public void clearAllOrder();
}
