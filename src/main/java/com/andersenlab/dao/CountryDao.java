package com.andersenlab.dao;

import java.util.List;
import com.andersenlab.model.Country;

public interface CountryDao {

  public Long createCountry(Country country);
  public List<Country> getAllCountry();
  public Country getCountry(Long id);
  public void updateCountry(Country country);
  public void deleteCountry(Long id);
  public void clearCountryList();
}
