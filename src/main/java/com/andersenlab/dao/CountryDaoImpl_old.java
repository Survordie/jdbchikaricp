package com.andersenlab.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import com.andersenlab.DBUtil;
import com.andersenlab.model.Country;



public class CountryDaoImpl_old implements CountryDao {

  private DataSource dataSource = null;
  private Connection connection = null;
  private PreparedStatement pstmt = null;
  private ResultSet resultSet = null;


  public CountryDaoImpl_old() {
    this.dataSource = DBUtil.getConnection();
  }

  @Override
  public Long createCountry(Country country) {
    try {
      connection = dataSource.getConnection();
      pstmt = connection.prepareStatement(
          "INSERT INTO country(country_code,country_name,active) VALUES(?,?,?)",
          Statement.RETURN_GENERATED_KEYS);

      pstmt.setString(1, country.getCountry_code());
      pstmt.setString(2, country.getCountry_name());
      pstmt.setBoolean(3, true);

      boolean execute = pstmt.execute();

      ResultSet generatedKeys = pstmt.getGeneratedKeys();
      
      while (generatedKeys.next()) {
        return generatedKeys.getLong(1);

      }      
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public List<Country> getAllCountry() {
    List<Country> listCountry = new ArrayList<>();
    try {
      connection = dataSource.getConnection();
      pstmt = connection.prepareStatement("SELECT * FROM country");
      resultSet = pstmt.executeQuery();

      while (resultSet.next()) {
        Country country = new Country();
        country.setId(resultSet.getLong(1));
        country.setCountry_code(resultSet.getString(2));
        country.setCountry_name(resultSet.getString(3) != null ? resultSet.getString(3) : "");
        country.setActive(resultSet.getBoolean(5));
        listCountry.add(country);

      }
    } catch (SQLException e) {

      e.printStackTrace();
    }
    return listCountry;
  }

  @Override
  public Country getCountry(Long id) {
    try {
      connection = dataSource.getConnection();
      pstmt = connection.prepareStatement("SELECT * FROM country WHERE id = ?");
      pstmt.setLong(1, id);
      resultSet = pstmt.executeQuery();
      while (resultSet.next()) {
        Country country = new Country();
        country.setId(resultSet.getLong(1));
        country.setCountry_code(resultSet.getString(2));
        country.setCountry_name(resultSet.getString(3) != null ? resultSet.getString(3) : "");
        country.setActive(resultSet.getBoolean(5));

        return country;

      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return null;
  }

  @Override
  public void updateCountry(Country country) {
    try {
      connection = dataSource.getConnection();
      pstmt = connection.prepareStatement(
          "UPDATE country SET country_code = ?, country_name = ?, active = ?  WHERE id = ?");
      pstmt.setString(1, country.getCountry_code().isEmpty() ? "ru" : country.getCountry_code());
      pstmt.setString(2, country.getCountry_name());
      pstmt.setBoolean(3, country.getActive() == null ? false : country.getActive());
      pstmt.setLong(4, country.getId());

      pstmt.executeUpdate();
    } catch (SQLException e) {

      e.printStackTrace();
    }

  }

  @Override
  public void deleteCountry(Long id) {

    try {
      connection = dataSource.getConnection();
      pstmt = connection.prepareStatement("DELETE FROM country WHERE id = ?");
      pstmt.setLong(1, id);

      pstmt.execute();
    } catch (SQLException e) {

      e.printStackTrace();
    }
  }

  @Override
  public void clearCountryList() {
    try {
      connection = dataSource.getConnection();
      pstmt = connection.prepareStatement("DELETE FROM country");

      pstmt.execute();
    } catch (SQLException e) {

      e.printStackTrace();
    }

  }

}
