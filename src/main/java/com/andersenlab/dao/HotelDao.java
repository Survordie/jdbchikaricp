package com.andersenlab.dao;

import java.util.List;
import com.andersenlab.model.Hotel;

public interface HotelDao {

	public Long createHotel(Hotel hotel);
	public List<Hotel> getAllHotel();
	public Hotel getHotelById(Long id);
	public void updateHotel(Hotel hotel);
	public void deleteHotelById(Long id);
	public void clearAllHotel();
}
