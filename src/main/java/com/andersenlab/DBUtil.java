package com.andersenlab;

import javax.sql.DataSource;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DBUtil {

  public static final String DB_USERNAME = "postgres";
  public static final String DB_PASSWORD = "postgres";
  public static final String DB_URL = "jdbc:postgresql://localhost:5432/tagency";
  public static final String DS_CLASSNAME = "org.postgresql.ds.PGSimpleDataSource";

  private static DataSource dataSource;

  public static DataSource getConnection() {

    if (dataSource == null) {
      HikariConfig config = new HikariConfig();
      config.setJdbcUrl(DB_URL);
      config.setUsername(DB_USERNAME);
      config.setPassword(DB_PASSWORD);

      config.setMaximumPoolSize(10);
//      config.setAutoCommit(false);
      config.addDataSourceProperty("cachePrepStmts", "true");
      config.addDataSourceProperty("prepStmtCacheSize", "250");
      config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
      
      dataSource = new HikariDataSource(config);
    }
    return dataSource;

  }
}
